#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/13540000.dwmmc0/by-name/RECOVERY:10661888:278dc629cffd61987147587a4192d69c181c0242; then
  applypatch EMMC:/dev/block/platform/13540000.dwmmc0/by-name/BOOT:8415232:4e29ba62985f7e3f6393ece14b4c4cb8b5392fe0 EMMC:/dev/block/platform/13540000.dwmmc0/by-name/RECOVERY 278dc629cffd61987147587a4192d69c181c0242 10661888 4e29ba62985f7e3f6393ece14b4c4cb8b5392fe0:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
