## o5proltedd-user 7.1.1 NMF26X G550FYXXU1CRD1 release-keys
- Manufacturer: samsung
- Platform: exynos3
- Codename: o5prolte
- Brand: samsung
- Flavor: o5proltedd-user
- Release Version: 7.1.1
- Id: NMF26X
- Incremental: G550FYXXU1CRD1
- Tags: release-keys
- CPU Abilist: armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 320
- Fingerprint: samsung/o5proltedd/o5prolte:7.1.1/NMF26X/G550FYXXU1CRD1:user/release-keys
- OTA version: 
- Branch: o5proltedd-user-7.1.1-NMF26X-G550FYXXU1CRD1-release-keys
- Repo: samsung_o5prolte_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
